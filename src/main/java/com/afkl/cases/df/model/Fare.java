package com.afkl.cases.df.model;


import lombok.Value;

@Value
public class Fare {
    Double amount;
    String origin;
    String destination;

    public Fare() {}
   }
