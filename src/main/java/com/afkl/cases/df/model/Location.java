package com.afkl.cases.df.model;

import lombok.Value;

@Value
public class Location {

    private String code;
    private String name;
    private String description;
    

 
}
