package com.afkl.cases.df.service;

import java.util.List;

import com.afkl.cases.df.dto.FlightResult;
import com.afkl.cases.df.exception.FlightSearchException;

public interface ISearchFlightFare {
	
	public abstract List<FlightResult> fetchFlightFare(String origin,String destination) ;
	
	

}
