package com.afkl.cases.df.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.afkl.cases.df.common.ApplicationConstants;
import com.afkl.cases.df.common.OAuthAuthorization;
import com.afkl.cases.df.common.Utils;
import com.afkl.cases.df.dto.FlightResult;
import com.afkl.cases.df.exception.FlightSearchException;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.Location;

@Component
public class SearchFlightFareImpl implements ISearchFlightFare {

	@Autowired
	private Environment env;

	@Override
	public List<FlightResult> fetchFlightFare(String origin, String destination)  {

		FlightResult flightResultDTO = new FlightResult();
		List<FlightResult> flightFareList = new ArrayList<>();
		try {
			// Retrieve accesstoken
			String accessToken = OAuthAuthorization.getAccessToken();
			//
			if (accessToken != null) {

				CompletableFuture<Void> task1 = CompletableFuture.runAsync(() -> {
					try {
						flightResultDTO.setFare(fetchFare(origin, destination, accessToken));
					} catch (IOException e) {
						System.out.println(e.getMessage());
					}

				});

				CompletableFuture<Void> task2 = CompletableFuture.runAsync(() -> {
					try {
						flightResultDTO.setOrigin(fetchLocation(origin, accessToken));
					} catch (IOException e) {
						System.out.println(e.getMessage());
					}
				});

				CompletableFuture<Void> task3 = CompletableFuture.runAsync(() -> {
					try {
						flightResultDTO.setDestination(fetchLocation(destination, accessToken));
					} catch (IOException e) {
						System.out.println(e.getMessage());
					}
				});

				CompletableFuture<Void> executeTasks = CompletableFuture.allOf(task1, task2, task3);

				try {
					executeTasks.get(); // this line waits for all to be completed

					flightFareList.add(flightResultDTO);
				} catch (InterruptedException | ExecutionException e) {
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return flightFareList;
	}

	private Fare fetchFare(String origin, String destination, String accessToken) throws IOException {
		Fare farevo = new Fare();

		String location = origin + "/" + destination;
		farevo = (Fare) Utils.getRestApiCall(
				env.getProperty(ApplicationConstants.BASE_URL) + ApplicationConstants.FARE + location, accessToken);

		return farevo;
	}

	private Location fetchLocation(String locationCode, String accessToken) throws IOException {

		Location locationVO = new Location();

		locationVO = (Location) Utils.getRestApiCall(
				env.getProperty(ApplicationConstants.BASE_URL) + ApplicationConstants.AIRPORTS + locationCode,
				accessToken);

		return locationVO;

	}

}
