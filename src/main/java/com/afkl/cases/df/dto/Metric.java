package com.afkl.cases.df.dto;
import lombok.Value;

@Value
public class Metric {
	
	private String httpCode;
	private int count;
	

}
