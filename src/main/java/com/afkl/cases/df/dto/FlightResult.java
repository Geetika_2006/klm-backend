package com.afkl.cases.df.dto;

import java.io.Serializable;

import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.Location;
import lombok.Value;

@Value
public class FlightResult implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7435925054675279584L;

	private Fare fare;
	private Location origin;
    private Location destination;
   
	
	
}
