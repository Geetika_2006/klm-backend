package com.afkl.cases.df;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import com.afkl.cases.df.controller.MetricController;

@SpringBootApplication
public class Bootstrap extends SpringBootServletInitializer {


	public Bootstrap() {
		super();
		setRegisterErrorPageFilter(false);

	}

	public static void main(String[] args) {
		SpringApplication.run(Bootstrap.class, args);
		
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MetricController.class);
	}

	public void run(String... arg0) throws Exception {
		
		
	}

	
	
	
	
	
	

}
