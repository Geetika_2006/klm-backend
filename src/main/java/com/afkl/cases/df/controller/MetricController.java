package com.afkl.cases.df.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.common.ApplicationConstants;
import com.afkl.cases.df.common.Utils;
import com.afkl.cases.df.dto.Metric;
import com.afkl.cases.df.exception.MetricException;

@Configuration
@EnableAutoConfiguration
@RestController
public class MetricController {

	@Autowired
	private Environment env;

	/**
	 * This rest end point is for fetching metrics
	 * 
	 * @return
	 * @throws MetricException
	 */
	@RequestMapping(value = "/statusMetric", method = RequestMethod.GET)
	public List<Metric> statusMetric() throws MetricException {
		// status , count
		HashMap<String, Integer> statusMap = new HashMap<String, Integer>();
		List<Metric> metricList = new ArrayList<>();
		try {
			Object obj = Utils.getMetrics(
					env.getProperty(ApplicationConstants.METRIC_URL) + ApplicationConstants.TRACE_END_POINT);
			int count = 0;
			JSONArray array = (JSONArray) obj;
			for (int j = 0; j < array.size(); j++) {

				JSONObject object = (JSONObject) array.get(j);
				JSONObject infoObject = (JSONObject) object.get(ApplicationConstants.INFO_JSON);
				JSONObject header = (JSONObject) infoObject.get(ApplicationConstants.HEADERS_JSON);
				JSONObject response = (JSONObject) header.get(ApplicationConstants.RESPONSE_JSON);
				String status = response.get(ApplicationConstants.STATUS_JSON).toString();
				statusMap.put(status, count++);
			}

			for (Entry<String, Integer> h : statusMap.entrySet()) {

				Metric metricDTO = new Metric();
				metricDTO.setHttpCode(h.getKey());
				metricDTO.setCount(h.getValue());
				metricList.add(metricDTO);
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return metricList;
	}
}
