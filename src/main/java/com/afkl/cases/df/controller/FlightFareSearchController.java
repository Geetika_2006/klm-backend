package com.afkl.cases.df.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.common.ApplicationConstants;
import com.afkl.cases.df.common.OAuthAuthorization;
import com.afkl.cases.df.common.Utils;
import com.afkl.cases.df.dto.Airport;
import com.afkl.cases.df.dto.FlightResult;
import com.afkl.cases.df.exception.FlightSearchException;
import com.afkl.cases.df.service.ISearchFlightFare;

@Configuration
@EnableAutoConfiguration
@RestController
public class FlightFareSearchController {

	@Autowired
	private Environment env;

	@Autowired
	private ISearchFlightFare searchFlightFare;

	/**
	 * Rest Endpoint to fetch flight fares providing origin and destination
	 * 
	 * @param origin
	 * @param destination
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fares/{origin}/{destination}", method = RequestMethod.GET)
	public List<FlightResult> fetchFare(@PathVariable("origin") String origin,
			@PathVariable("destination") String destination) {
		return searchFlightFare.fetchFlightFare(origin, destination);
	}

	/**
	 * Rest Endpoint to fetch airports from code
	 * 
	 * @param code
	 * @return
	 * @throws FlightSearchException
	 */
	@RequestMapping(value = "/airports/{code}", method = RequestMethod.GET)
	public Airport fetchAirports(@PathVariable("code") String code) throws FlightSearchException {

		Airport airports = new Airport();
		// Retrieve token
		String accessToken = OAuthAuthorization.getAccessToken();

		if (accessToken != null) {
			try {
				airports = (Airport) Utils.getRestApiCall(
						env.getProperty(ApplicationConstants.BASE_URL) + ApplicationConstants.AIRPORTS + code,
						accessToken);
			} catch (Exception e) {
				e.getMessage();
			}
		}

		return airports;

	}

}
