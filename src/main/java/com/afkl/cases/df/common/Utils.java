package com.afkl.cases.df.common;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.simple.parser.JSONParser;

public class Utils {

	
	
	public static Object getRestApiCall(String URL, String accessToken)
			throws MalformedURLException, IOException, ProtocolException {

		JSONParser parser = new JSONParser();
		Object obj = null;
		URL apiUrl = new URL(URL);

		HttpURLConnection connection= (HttpURLConnection) apiUrl.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod(ApplicationConstants.HTTP_GET);
		String auth = OAuthConstants.BEARER + accessToken;
		connection.setRequestProperty(OAuthConstants.CONTENT_TYPE, OAuthConstants.JSON_CONTENT);
		connection.setRequestProperty(ApplicationConstants.AUTHORIZATION, auth);

		DataInputStream dataInputStream = new DataInputStream(connection.getInputStream());
		int x = 0;
		StringBuilder streamBuilder = new StringBuilder();
		while ((x = dataInputStream.read()) != -1) {
			streamBuilder.append((char) x);
		}

		try {
			obj = parser.parse(streamBuilder.toString());
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}

		dataInputStream.close();
		connection.disconnect();

		return obj;
	}

	public static Object getMetrics(String URL)
			throws MalformedURLException, IOException, ProtocolException {

		JSONParser parser = new JSONParser();
		Object obj = null;
		URL apiUrl = new URL(URL);

		HttpURLConnection connection= (HttpURLConnection) apiUrl.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod(ApplicationConstants.HTTP_GET);
		connection.setRequestProperty(OAuthConstants.CONTENT_TYPE, OAuthConstants.JSON_CONTENT);

		DataInputStream dataInputStream = new DataInputStream(connection.getInputStream());
		int x = 0;
		StringBuilder streamBuilder = new StringBuilder();
		while ((x = dataInputStream.read()) != -1) {
			streamBuilder.append((char) x);
		}

		try {
			obj = parser.parse(streamBuilder.toString());
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		dataInputStream.close();
		connection.disconnect();

		return obj;
	}

	
}
