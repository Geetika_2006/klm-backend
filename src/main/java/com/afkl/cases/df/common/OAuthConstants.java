package com.afkl.cases.df.common;


public final class OAuthConstants {
	
	public static final String CLIENT_ID = "travel-api-client";
	public static final String CLIENT_SECRET = "psw";
	public static final String AUTHENTICATION_SERVER_URL = "http://localhost:8080/oauth/token";
	public static final String GRANT_TYPE = "grant_type";
	public static final String GRANT_TYPE_PASSWORD = "password";
	public static final String GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";
	public static final String SCOPE = "scope";
	public static final String AUTHORIZATION = "Authorization";
	public static final String BEARER = "Bearer ";
	public static final String BASIC = "Basic";
	public static final String JSON_CONTENT = "application/json";
	public static final String CONTENT_TYPE="content-Type";
	
	

	public static final int HTTP_UNAUTHORIZED = 401;
	 
	

}
