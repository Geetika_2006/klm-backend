package com.afkl.cases.df.common;

/**
 * Source: IBM tutorial OAuth 2.0
 */
public final class ApplicationConstants {
	
	public static final String BASE_URL="base.url";
	public static final String METRIC_URL="metric.url";
	public static final String FARE ="fares/";
	public static final String AIRPORTS ="airports/";
	public static final String TRACE_END_POINT ="travel/trace";
	public static final String INFO_JSON ="info";
	public static final String HEADERS_JSON ="headers";
	public static final String RESPONSE_JSON ="response";
	public static final String STATUS_JSON="status";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String HTTP_GET = "GET";
	public static final String AUTHORIZATION="Authorization";
	
	
	
	
	
	
	
}